from referrals.models import Referral
from referrals.serializers import ReferralSerializer
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import get_object_or_404


class ReferralViewSet(viewsets.ModelViewSet):
    """
    Retrieve, update or delete a referral instance.
    """
    queryset = Referral.objects.all().order_by('-created_at')
    serializer_class = ReferralSerializer
    lookup_field = 'title'

    def retrieve(self, request, title=None):
        referral = self.get_object()
        serializer = ReferralSerializer(referral, context={'request': request})
        return Response(serializer.data)
        # url = reverse('referral-landing')+'?link='+title
        # return HttpResponseRedirect(redirect_to=url)

class LandingView(APIView):
    def get(self, request, format=None):
        referral = get_object_or_404(Referral, title=request.query_params.get('link'))
        referral.clicks += 1
        referral.save()
        serializer = ReferralSerializer(referral, context={'request': request})
        return Response(serializer.data)