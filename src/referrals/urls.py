from django.urls import include, path
from rest_framework import routers
from referrals import views

router = routers.DefaultRouter(trailing_slash=False)
router.register(r'', views.ReferralViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('landing/', views.LandingView.as_view(), name='referral-landing'),
]