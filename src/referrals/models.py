from django.utils import timezone
from django.db import models

# Create your models here.
class Referral(models.Model):
    title      = models.CharField(max_length=255, blank=False)
    clicks     = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('created_at',)
