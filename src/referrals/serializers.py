from referrals.models import Referral
from rest_framework import serializers

class ReferralSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Referral
        fields = ('url', 'id', 'title', 'clicks')
        read_only_fields = ('id', 'clicks')
        extra_kwargs = {
            'url': {'lookup_field': 'title'}
        }