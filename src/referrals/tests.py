from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from referrals.models import Referral

class ReferralTests(APITestCase):

    def test_create_referral(self):
        """
        Ensure we can create a new referral.
        """
        url = reverse('referral-list')
        data = {'title': 'test'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(response.data), 4)
        self.assertEqual(Referral.objects.count(), 1)
        self.assertEqual(Referral.objects.get().title, 'test')
        """
        Ensure click increments when link visited
        """
        url = reverse('referral-landing')+'?link=test'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['clicks'], 1)
        """
        Ensure referral can be edited
        """
        url = reverse('referral-detail', kwargs={'title':'test'})
        data = {'title': 'test123'}
        response = self.client.put(url, data,  format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['title'], 'test123')
        """
        Ensure referral can be deleted
        """
        url = reverse('referral-detail', kwargs={'title':'test123'})
        response = self.client.delete(url,  format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        url = reverse('referral-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 0)