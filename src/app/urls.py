from django.urls import include, path
from rest_framework import routers
from referrals import views
#@TODO: use file server container for serving static files
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

# Use simple router for this situation
# router = routers.DefaultRouter()
# router = routers.SimpleRouter()
# router.register(r'', views.ReferralViewSet, basename='Referrals')

# Wire up our API using automatic URL routing.
urlpatterns = [
    # path('', include(router.urls)),
    path('', include('referrals.urls')),
]
#@TODO: use file server container for serving static files
urlpatterns += staticfiles_urlpatterns()