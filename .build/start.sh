#!/usr/bin/env bash

#USAGE:
#./start.sh --env localhost
#./start.sh --env prod

if [ -z "$1" ] || [ -z "$2" ]; then
    echo "Must set env (--env localhost)"
    exit 1
fi

if [ -n "$1" ]; then
    case "$1" in
      -e|--env)
          env=$2;;
    esac
fi

if [ "$env" == "localhost" ]; then

    docker-compose -f ../docker-compose.yml.localhost up --build --force-recreate

elif [ "$env" == "prod" ]; then

    docker-compose -f ../docker-compose.yml.prod up --build --force-recreate

fi
