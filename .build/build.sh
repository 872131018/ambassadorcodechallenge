#!/usr/bin/env bash

# create staging area
mkdir staging/

git clone git@gitlab.com:872131018/AmbassadorCodeChallenge.git staging/

cd staging/src/

# Build and push docker images
docker login registry.gitlab.com

docker-compose --file ../docker-compose.yml.build build --no-cache

docker-compose -f ../docker-compose.yml.build push

# Clean up staging directory
docker system prune --all --force

cd ../../

rm -rf staging/
