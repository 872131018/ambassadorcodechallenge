# John's Code Challenge

## Description
Build a CRUD API that supports some basic functionality for a webpage.  Things this API should do
1. Display a list of links and how many times they have been clicked with a GET request.
2. Create a new link with a POST request with the title as the request body.
3. Retrieve a link using its title as a query string param and increment its click count.
4. Update a link by providing a new title as the body of a PUT request.
5. Delete a link by passing a DELETE request with the title as a query string param.

## Solution
This project contained several firsts for me.  I have done a little Django in the past but I haven't used the Django REST framework.  I was pleased at the number of features it included and found the browsable API to be a really great tool.  I did find the docs and informational sections to be somehwat vague, often excluding the reasons for why a class has a certain feature or what a feature really does. However, it was a really positive learning experience overall.  Deploying with Heroku was also a first for me.  I chose to deploy to Heroku with the docker images I was building locally and once I discovered the secret recipe it was really simple.  Heroku was a good learning experience as well and is good to know because of how great docker can be as a development environment.  I typically use docker for all my work because it allows me a great deal of flexibility when switching between projects to moving between different computers.

## Hindsight
While I could have powered through the full stack requirements of the challenge pretty quickly using a toolkit I'm comfortable with (such as Laravel and Vue on an AWS EC2) I chose to try the recommended options and learn more about Django REST and Heroku.  I would have liked to dive deeper into the browsable API because I read about some really neat features it has such as deep linking and form customization.  It would have been good to figure out how to seamlessly redirect to the landing page from a custom clickable url property on the model.

## Hostname
https://whispering-plains-13178.herokuapp.com

## Recommended Workflow
Here I will outline the steps I used to verify the functionality of the API
1. Navigate to https://whispering-plains-13178.herokuapp.com to see a list of links
2. At the bottom of the page you will have a form to add links.  Add the link "spartans" and POST it
3. Towards the top of the page click the "List Create Api" link to return to the list of links
4. Verify your "spartans" link has been posted and has zero clicks
5. Click the "url" property on your spartan link to navigate to the edit actions
6. In the form at the bottom of the page enter the json "spartan123"` and PUT the request
8. Verify you have updated the title of the spartan link to "spartan123"
9. To go to to the links landing page and register a click navigate to
`https://whispering-plains-13178.herokuapp.com/landing/?link=spartan123`
10. Verify the link now has a click
11. Return to the link update page by clicking through the Browsable API
12. To delete this link, click the red button in the top right to send DELETE request for this link
13. Verify the link has been deleted when landing page reloads with a 404 Not Found code
14. Return to the "List Create Api" page and verify the "spartan123" link is no longer in the results.

